import csv
import logging

from django.db import DataError

from book_app.models import Book


logging.basicConfig(filename='errors.log', level=logging.DEBUG)


class SaveFile():
    @staticmethod
    def open_file(file):
        with open(file) as f:
            reader = csv.reader(f, delimiter='|')
            file = []

            for line in reader:
                file.append(line)

            return file

    @staticmethod
    def read_line(line):
        ext_id = line[0]
        name = line[1]
        description = line[2]
        return ext_id, name, description

    @staticmethod
    def save_line(ext_id, name, description):
        try:
            book = Book.objects.get(ext_id=ext_id)
        except Book.DoesNotExist:
            book = Book(
                ext_id=ext_id,
                name=name,
            )
        book.description = description

        try:
            book.save()
        except DataError:
            msg = 'DataError: Could not import element ext_id=' + ext_id + ', name=' + name
            logging.error(msg)
