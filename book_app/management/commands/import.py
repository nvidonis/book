from django.core.management.base import BaseCommand  # , CommandError

from helper import SaveFile


class Command(BaseCommand):
    help = 'Imports data from Excel'

    def handle(self, *args, **options):

        file = SaveFile.open_file('book_import_file.txt')  # lista u listi

        for line in file:
            ext_id, name, description = SaveFile.read_line(line)
            SaveFile.save_line(ext_id, name, description)
