from django.contrib import admin

from .models import Book, Author


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ['ext_id', 'name', 'description']
    raw_id_fiels = ['author__name']
    list_filter = ['author__name']


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ['name']
