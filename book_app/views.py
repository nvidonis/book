from django.views.generic import ListView, UpdateView, CreateView, DeleteView

from api.views import *
from .models import Book, Author


class BookList(ListView):
    model = Book
    template_name = 'book_app/book_list.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BookList, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['book'] = Book.objects.all().order_by('pk').select_related()
        context['author'] = Author.objects.all().order_by('name')
        return context


class BookUpdate(UpdateView):
    model = Book
    fields = ['name', 'description', 'author']
    success_url = '/book/'


class BookCreate(CreateView):
    model = Book
    fields = ['name', 'description', 'author']
    success_url = '/book/'


class BookDelete(DeleteView):
    model = Book
    success_url = '/book/'


class AuthorUpdate(UpdateView):
    model = Author
    fields = ['name']
    success_url = '/book/'


class AuthorCreate(CreateView):
    model = Author
    fields = ['name']
    success_url = '/book/'


class AuthorDelete(DeleteView):
    model = Author
    success_url = '/book/'
