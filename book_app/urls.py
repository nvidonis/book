from django.conf.urls import url, include

import api.urls

from book_app.views import BookList, BookUpdate, AuthorUpdate
from book_app.views import AuthorCreate, BookCreate, AuthorDelete, BookDelete

from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    url(r'^api/', include(api.urls)),
    url(r'^$', BookList.as_view(), name='book2'),
    url(r'^(?P<pk>[0-9]+)/$', BookUpdate.as_view(), name='book_update'),
    url(
        r'^author/(?P<pk>[0-9]+)/$',
        AuthorUpdate.as_view(),
        name='author_update'),
    url(r'^author-create/$', AuthorCreate.as_view(), name='author_create'),
    url(r'^book-create/$', BookCreate.as_view(), name='book_create'),
    url(
        r'^author-delete-(?P<pk>[0-9]+)/$',
        AuthorDelete.as_view(),
        name='author_delete'),
    url(
        r'^book-delete-(?P<pk>[0-9]+)/$',
        BookDelete.as_view(),
        name='book_delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json'])
