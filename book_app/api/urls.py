from django.conf.urls import url

from views import BookListViewApi, BookDetailViewApi, BookListApi, BookDetailApi
from views import AuthorDetailApi

# from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^book/$', BookListApi.as_view(), name='book_list_api'),
    url(r'^books/$', BookListViewApi.as_view()),

    url(
        r'^books/(?P<pk>[0-9]+)/$',
        BookDetailViewApi.as_view(),
        name='books_detail_api'),
    url(
        r'^book/(?P<pk>[0-9]+)/$',
        BookDetailApi.as_view(),
        name='book_detail_api'),

    url(
        r'^author/(?P<pk>[0-9]+)/$',
        AuthorDetailApi.as_view(),
        name='author_detail_api'),
]

# urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json'])
