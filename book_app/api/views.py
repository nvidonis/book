
from django.contrib.auth.models import User, Group
from django.shortcuts import get_object_or_404, redirect

from rest_framework import generics
from rest_framework import viewsets
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import Book, Author
from ..serializers import UserSerializer, GroupSerializer, BookSerializer, AuthorSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class BookListViewApi(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookDetailViewApi(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookListApi(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'book_app/api/book_list.html'

    def get(self, request):
        queryset = Book.objects.all().order_by('pk').select_related()
        author = Author.objects.all().order_by('name')
        return Response(
            {'book': queryset, 'author': author}
        )


class BookDetailApi(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'book_app/api/book_detail.html'

    def get(self, request, pk):
        book = get_object_or_404(Book, pk=pk)
        serializer = BookSerializer(book)
        return Response({'serializer': serializer, 'book': book})

    def post(self, request, pk):
        book = get_object_or_404(Book, pk=pk)
        serializer = BookSerializer(book, data=request.data)
        if not serializer.is_valid():
            return Response({'serializer': serializer, 'book': book})
        serializer.save()
        return redirect('book_list_api')


class AuthorDetailApi(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'book_app/api/author_detail.html'

    def get(self, request, pk):
        author = get_object_or_404(Author, pk=pk)
        serializer = AuthorSerializer(author)
        return Response({'serializer': serializer, 'author': author})

    def post(self, request, pk):
        author = get_object_or_404(Author, pk=pk)
        serializer = AuthorSerializer(author, data=request.data)
        if not serializer.is_valid():
            return Response({'serializer': serializer, 'author': author})
        serializer.save()
        return redirect('book_list_api')
