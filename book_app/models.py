# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Author(models.Model):
    name = models.CharField("Name", max_length=155)

    class Meta:
        verbose_name = "Author"
        verbose_name_plural = "Authors"

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Book(models.Model):
    author = models.ManyToManyField(Author)
    ext_id = models.CharField("External id", max_length=5)
    name = models.CharField("Name", max_length=20)
    description = models.TextField("Description")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Book"
        verbose_name_plural = "Books"
